export class Barquito extends HTMLElement {
  constructor() {
    super()
    this.innerHTML = `
      <svg width="800" height="600">
        <g>
          <line stroke-linecap="undefined" stroke-linejoin="undefined" id="svg_2" y2="354" x2="579.5" y1="354" x1="235.5"
            fill-opacity="null" stroke-opacity="null" stroke-width="20.5" stroke="#000" fill="none" />
          <ellipse ry="5.5" rx="160.5" id="svg_7" cy="362.5" cx="407.99999" fill-opacity="null" stroke-opacity="null"
            stroke-width="20.5" stroke="#000" fill="none" />
          <line transform="rotate(-1, 403, 246)" stroke-linecap="null" stroke-linejoin="null" id="svg_8" y2="140.5" x2="404"
            y1="351.5" x1="402" fill-opacity="null" stroke-opacity="null" stroke-width="20.5" stroke="#000" fill="none" />
          <path id="svg_12" d="m286.5,314" opacity="0.5" fill-opacity="null" stroke-opacity="null" stroke-width="20.5"
            stroke="#000" fill="none" />
          <line stroke-linecap="undefined" stroke-linejoin="undefined" id="svg_9" y2="229" x2="554.5" y1="148" x1="405.5"
            fill-opacity="null" stroke-opacity="null" stroke-width="20.5" stroke="#000" fill="none" />
          <line stroke-linecap="undefined" stroke-linejoin="undefined" id="svg_10" y2="224" x2="549.5" y1="311" x1="407.5"
            fill-opacity="null" stroke-opacity="null" stroke-width="20.5" stroke="#000" fill="none" />
          <path id="svg_14" fill-opacity="null" stroke-opacity="null" stroke-width="20.5" stroke="#000" fill="none" />
          <path id="svg_15"
            d="m-1.5,388c72,-1 64,-21 71,-32c7,-11 13,26 36,24c23,-2 33,-44 42,-36c9,8 44,81 48,53c4,-28 20,-71 34,-38c14,33 34,63 39,39c5,-24 -3,-68 11,-58c14,10 30,26 37,42c7,16 34,32 37,17c3,-15 10,-55 20,-51c10,4 30,21 34,41c4,20 32,18 34,5c2,-13 -2,-60 20,-49c22,11 36,49 51,58c15,9 51,2 52,-13c1,-15 7,-63 23,-43c16,20 27,43 31,49c4,6 33,0 40,-16c7,-16 3,-57 20,-36c17,21 32,65 39,60c7,-5 18,-23 22,-36c4,-13 7,-40 28,-13c21,27 20,51 30,42c10,-9 30,-32 34,-39c4,-7 43,-7 44,2c1,9 73,-1 72.5,-1"
            fill-opacity="null" stroke-opacity="null" stroke-width="20.5" stroke="#000" fill="none" />
          <ellipse ry="10" rx="151" id="svg_16" cy="377" cx="402.5" fill-opacity="null" stroke-opacity="null"
            stroke-width="20.5" stroke="#000" fill="none" />
          <ellipse ry="8" rx="155" id="svg_17" cy="393" cx="402.5" fill-opacity="null" stroke-opacity="null"
            stroke-width="20.5" stroke="#000" fill="none" />
        </g>
      </svg>
    `
    Array.from(this.querySelectorAll('[stroke]')).forEach(e => {
      e.setAttribute('stroke', this.dataset.color)
    })

  }

}
customElements.define('mi-barquito', Barquito)

export default new Barquito()